<?php

namespace Tests\NotificationBundle\Entity;

use NotificationBundle\Entity\Notification;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use UserBundle\Entity\User;

class NotificationTest extends KernelTestCase
{
    /** @var Notification */
    private $entity;

    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        $this->entity = new Notification();
    }

    public function testGetId()
    {
        $this->entity->setId(1);

        $this->assertEquals(1, $this->entity->getId());
    }

    public function testGetUser()
    {
        $user = new User();

        $this->entity->setUser($user);

        $this->assertSame($user, $this->entity->getUser());
    }

    public function testGetNotificationType()
    {
        $this->entity->setNotificationType('foo');

        $this->assertEquals('foo', $this->entity->getNotificationType());
    }

    public function testGetNotificationId()
    {
        $this->entity->setNotificationId(1);

        $this->assertEquals(1, $this->entity->getNotificationId());
    }
}