<?php

namespace Tests\NotificationBundle\Repository;

use Doctrine\ORM\EntityManager;
use NotificationBundle\Entity\Notification;
use NotificationBundle\Repository\NotificationRepository;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\DependencyInjection\Container;
use UserBundle\Entity\User;

class NotificationRepositoryTest extends KernelTestCase
{
    /** @var Container */
    private $container;

    /** @var EntityManager */
    private $em;

    /** @var NotificationRepository */
    private $repository;

    protected function setUp()
    {
        self::bootKernel();

        $this->container = static::$kernel->getContainer();
        $this->em = $this->container->get('doctrine.orm.entity_manager');
        $this->repository = $this->em->getRepository(Notification::class);
    }

    public function testGetNotifications()
    {
        $notifications = $this->repository->getNotifications();
    }

    public function testGetReadNotifications()
    {
    }

    public function testGetUnreadNotifications()
    {
    }
}