<?php

namespace NotificationBundle;

use \NotificationBundle\Contracts\NotificationLoggerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\Event;

class NotificationLogger implements NotificationLoggerInterface
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * Create a new notification logger instance.
     *
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @inheritdoc
     */
    public function log($message)
    {
        $this->logger->info($message);
    }

    /**
     * @inheritdoc
     */
    public function success(Event $event)
    {
        $this->logger->info('Notification was sent', $this->toArray($event));
    }

    /**
     * @inheritdoc
     */
    public function error(Event $event)
    {
        $this->logger->critical('Notification was not sent', $this->toArray($event));
    }

    /**
     * Converts the event to an array in order to log it.
     *
     * @param Event $event
     *
     * @return array
     */
    private function toArray(Event $event)
    {
        try {

            $array = [
                'notificationType' => (new \ReflectionClass($event->notification))->getShortName(),
                'channel' => $event->channel,
            ];

            if (is_object($event->notifiable)) {
                $array['notifiableId'] = $event->notifiable->getId();
                $array['notifiableType'] = (new \ReflectionClass($event->notifiable))->getShortName();
            } else {
                $array['notifiable'] = $event->notifiable;
            }

            return $array;

        } catch (\ReflectionException $exception) {
            die('The given event does not seem to be a valid event class.');
        }
    }
}