<?php

namespace NotificationBundle\Contracts;

interface ChannelManagerInterface
{
    /**
     * Get a driver instance.
     *
     * @param string $driver
     *
     * @return mixed
     */
    public function driver($driver = null);

    /**
     * Create a new driver instance.
     *
     * @param string $driver
     * @return mixed
     */
    public function createDriver($driver);

    /**
     * Get all of the created drivers.
     *
     * @return array
     */
    public function getDrivers();
}