<?php

namespace NotificationBundle\Contracts;

interface NotificationInterface
{
    /**
     * Get the notification channel.
     *
     * @param mixed $notifiable
     *
     * @return string|array
     */
    public function via($notifiable);
}