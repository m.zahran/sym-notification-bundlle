<?php

namespace NotificationBundle;

use Doctrine\Common\Collections\ArrayCollection;
use NotificationBundle\Events\NotificationSent;
use NotificationBundle\Events\NotificationFailed;
use NotificationBundle\Contracts\ShouldQueue;
use NotificationBundle\Contracts\ChannelInterface;
use NotificationBundle\Contracts\NotificationInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use NotificationBundle\Contracts\RabbitMQManagerInterface;

class NotificationSender
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var ChannelManager
     */
    protected $manager;

    /**
     * @var EventDispatcherInterface
     */
    protected $dispatcher;

    /**
     * @var RabbitMQManagerInterface
     */
    private $rabbitMQ;

    /**
     * Create a new instance of the notification sender.
     *
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->manager = $container->get('notification_channel_manager');
        $this->dispatcher = $container->get('event_dispatcher');
        $this->rabbitMQ = $container->get('notification_rabbitmq_manager');
    }

    /**
     * Send the given notification to the given notifiable entities.
     *
     * @param ArrayCollection|array|mixed $notifiables
     * @param NotificationInterface $notification
     *
     * @return void
     */
    public function send($notifiables, NotificationInterface $notification)
    {
        $notifiables = $this->formatNotifiables($notifiables);

        if ($notification instanceof ShouldQueue) {
            $this->queueNotification($notifiables, $notification);
            return;
        }

        $this->sendNow($notifiables, $notification);
    }

    /**
     * Send the given notification immediately.
     *
     * @param ArrayCollection|array|mixed $notifiables
     * @param NotificationInterface $notification
     * @param array|null $channels
     */
    public function sendNow($notifiables, NotificationInterface $notification, array $channels = null)
    {
        $notifiables = $this->formatNotifiables($notifiables);

        foreach ($notifiables as $notifiable) {

            if (empty ($viaChannels = $channels ?: $notification->via($notifiable))) {
                continue;
            }

            foreach ((array) $viaChannels as $channel) {
                $this->sendToNotifiable($notifiable, $notification, $channel);
            }
        }
    }

    /**
     * Send the given notification to the given notifiable via a channel.
     *
     * @param $notifiable
     * @param NotificationInterface $notification
     * @param $channel
     */
    protected function sendToNotifiable($notifiable, NotificationInterface $notification, $channel)
    {
        /** @var ChannelInterface $driver */
        $driver = $this->manager->driver($channel);

        $sent = $driver->send($notifiable, $notification);

        if ($sent) {
            $this->dispatcher->dispatch(
                'notification.sent',
                new NotificationSent($notifiable, $notification, $channel)
            );
        } else {
            $this->dispatcher->dispatch(
                'notification.failed',
                new NotificationFailed($notifiable, $notification, $channel)
            );
        }
    }

    /**
     * Queue the given notification instances.
     *
     * @param ArrayCollection|array|mixed $notifiables
     * @param NotificationInterface $notification
     */
    protected function queueNotification($notifiables, NotificationInterface $notification)
    {
        $notifiables = $this->formatNotifiables($notifiables);

        try {
            $notificationQualifiedName = (new \ReflectionClass($notification))->getName();
        } catch (\ReflectionException $e) {
            die('The given notification seems to be not a valid class.');
        }

        $this->rabbitMQ->openConnection();

        foreach ($notifiables as $notifiable) {
            foreach ((array) $notification->via($notifiables) as $channel) {

                try {
                    $notifiableQualifiedName = (new \ReflectionClass($notifiable))->getName();
                } catch (\ReflectionException $e) {
                    die('The given notification seems to be not a valid class.');
                }

                $this->rabbitMQ->sendMessage(json_encode([
                    'notificationType' => $notificationQualifiedName,
                    'notifiableType' => $notifiableQualifiedName,
                    'notifiableId' => $notifiable->getId(),
                    'channel' => $channel,
                ]));
            }
        }

        $this->rabbitMQ->closeConnection();
    }

    /**
     * Format the notifiables into an ArrayCollection/array if necessary.
     *
     * @param mixed $notifiables
     *
     * @return ArrayCollection|array
     */
    protected function formatNotifiables($notifiables)
    {
        if ( ! $notifiables instanceof ArrayCollection && ! is_array($notifiables)) {
            return [$notifiables];
        }

        return $notifiables;
    }
}