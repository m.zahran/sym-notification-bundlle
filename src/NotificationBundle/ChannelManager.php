<?php

namespace NotificationBundle;

use NotificationBundle\Channels\DatabaseChannel;
use NotificationBundle\Channels\MailChannel;
use NotificationBundle\Contracts\ChannelManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ChannelManager implements ChannelManagerInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * The default channel used to deliver notifications.
     *
     * @var string
     */
    protected $defaultChannel = 'database';

    /**
     * The array of created drivers.
     *
     * @var array
     */
    private $drivers;

    /**
     * Create a new channel manager instance.
     *
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @inheritdoc
     */
    public function driver($driver = null)
    {
        $driver = $driver ?: $this->getDefaultDriver();

        if ( ! isset($this->drivers[$driver])) {
            $this->drivers[$driver] = $this->createDriver($driver);
        }

        return $this->drivers[$driver];
    }

    /**
     * @inheritdoc
     */
    public function createDriver($driver)
    {
        $method = 'create' . ucfirst($driver) . 'Driver';

        if (method_exists($this, $method)) {
            return $this->$method();
        }

        throw new \InvalidArgumentException("Driver [$driver] not supported.");
    }

    /**
     * Get the default channel driver name.
     *
     * @return string
     */
    private function getDefaultDriver()
    {
        return $this->defaultChannel;
    }

    /**
     * @inheritdoc
     */
    public function getDrivers()
    {
        return $this->drivers;
    }

    /**
     * Create an instance of the database driver.
     *
     * @return DatabaseChannel
     */
    public function createDatabaseDriver()
    {
        return new DatabaseChannel($this->container->get('doctrine.orm.entity_manager'));
    }

    /**
     * Create an instance of the mail driver.
     *
     * @return MailChannel
     */
    public function createMailDriver()
    {
        return new MailChannel(
            $this->container->get('mailer'),
            $this->container->get('swiftmailer.transport.real')
        );
    }
}