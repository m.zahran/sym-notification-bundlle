<?php

namespace NotificationBundle\Repository;

use NotificationBundle\Entity\Notification;
use UserBundle\Entity\User;

class NotificationRepository extends \Doctrine\ORM\EntityRepository
{
    public function getNotifications(User $user)
    {

    }

    public function getReadNotifications(User $user)
    {

    }

    public function getUnreadNotifications(User $user)
    {

    }

    public function markAsRead(Notification $notification)
    {

    }

    public function markAllAsRead(User $user)
    {

    }
}
