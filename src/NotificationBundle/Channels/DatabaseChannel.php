<?php

namespace NotificationBundle\Channels;

use Doctrine\ORM\EntityManagerInterface;
use NotificationBundle\Contracts\ChannelInterface;
use NotificationBundle\Contracts\NotificationInterface;
use NotificationBundle\Entity\Notification as NotificationEntity;

class DatabaseChannel implements ChannelInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * Create a new database channel instance.
     *
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @inheritdoc
     */
    public function send($notifiable, NotificationInterface $notification)
    {
        $data = $this->getData($notifiable, $notification);

        $entity = (new NotificationEntity())
            ->setUser($notifiable)
            ->setNotificationId($data['notification_id'])
            ->setNotificationType($data['notification_type'])
            ->setCreatedAt(new \DateTime());

        $this->em->persist($entity);
        $this->em->flush();

        return true;
    }

    /**
     * @inheritdoc
     */
    public function getData($notifiable, NotificationInterface $notification)
    {
        if (method_exists($notification, 'toDatabase')) {
            return $notification->toDatabase($notifiable);
        }

        throw new \RuntimeException(sprintf(
            'Notification [%s] is missing "toDatabase method.',
            (new \ReflectionClass($notification))->getShortName()
        ));
    }
}