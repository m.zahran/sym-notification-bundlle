<?php

namespace NotificationBundle\Command;

use PhpAmqpLib\Connection\AMQPStreamConnection;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

class NotificationConsumerCommand extends ContainerAwareCommand {

    protected function configure()
    {
        $this->setName('aqarmap:notification-watch');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $notificationSender = $this->getContainer()->get('notification_sender');
        $config = $this->getContainer()->getParameter('aqarmap_notification')['rabbitmq'];

        $connection = new AMQPStreamConnection(
            $config['host'],
            $config['port'],
            $config['user'],
            $config['password'],
            $config['vhost'],
            false,      // insist
            'AMQPLAIN', // login_method
            null,       // login_response
            'en_US',    // locale
            3,
            3,
            null,
            false,
            0
        );

        $channel = $connection->channel();
        $channel->queue_declare($config['queue'], false, false, false, false);

        $callback = function ($message) use ($notificationSender, $em, $output) {

            $message = json_decode($message->body);

            $notifiable = $em->getReference($message->notifiableType, $message->notifiableId);
            $notification = new $message->notificationType($notifiable);

            $notificationSender->sendNow($notifiable, $notification, [$message->channel]);

            $output->writeln(sprintf("Send %s:%s [%s: %s]",
                $message->notifiableType,
                $message->notifiableId,
                $message->notificationType,
                $message->channel
            ));

        };

        $channel->basic_consume($config['queue'], '', false, true, false, false, $callback);

        while(count($channel->callbacks)) {
            $channel->wait();
        }
    }
}
